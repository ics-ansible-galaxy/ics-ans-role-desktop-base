ics-ans-role-desktop-base
=========================

Ansible role to install basic requirements for desktop applications.
It currently installs:
- common packages (see `desktop_base_packages` variable)
- ESS desktop menu and ESS links

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
desktop_base_packages:
  - xorg-x11-drivers
  - firefox
  - "@Xfce"
  - xfce4-whiskermenu-plugin
  - xfce4-screenshooter-plugin
  - pinta
  - ImageMagick
  - vinagre
  - recordmydesktop
  - gtk-recordmydesktop
  - pygtk2
  - gnome-screenshot
desktop_base_install_libreoffice: false
desktop_base_ess_links:
  - name: "JupyterHub"
    description: "ESS JupyterHub"
    url: https://jupyterhub.esss.lu.se
  - name: "Logbook"
    description: "ESS Logbook"
    url: https://logbook.esss.lu.se
  - name: "ESS Confluence"
    description: "ESS Confluence website"
    url: https://confluence.esss.lu.se
  - name: "JIRA CS-Studio"
    description: "CS-Studio JIRA project"
    url: https://jira.esss.lu.se/projects/CSSTUDIO
  - name: "JIRA OpenXAL"
    description: "OpenXAL JIRA project"
    url: https://jira.esss.lu.se/projects/OXAL
  - name: "JIRA ICS Infrastructure"
    description: "ICS Infrastructure JIRA project"
    url: https://jira.esss.lu.se/projects/INFRA
desktop_base_xfce_profile_reset: false
# XFCE profile to install: default (for LCR) or 1screen
desktop_base_xfce_profile: "1screen"
# set to true if you don't want the machine to reboot
# when the default.target is changed (this is for the devenv)
desktop_base_disable_reboot: false
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-desktop-base
```

License
-------

BSD 2-clause
