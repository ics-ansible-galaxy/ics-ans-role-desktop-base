import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_desktop_files(host):
    filenames = ('/usr/local/share/icons/ess-logo-square.png',
                 '/usr/share/desktop-directories/ESS.directory',
                 '/etc/xdg/menus/applications-merged/ESS.menu',
                 '/usr/share/desktop-directories/ESSLinks.directory',
                 '/etc/xdg/menus/applications-merged/ESSLinks.menu')
    for filename in filenames:
        file = host.file(filename)
        assert file.exists
        assert file.user == 'root'
        assert file.group == 'root'
        assert file.mode == 0o644


def test_firefox_installed(host):
    assert host.package('firefox').is_installed


def test_xfce_screenshot_installed(host):
    assert host.package('xfce4-screenshooter').is_installed
    assert host.file('/usr/bin/xfce4-screenshooter').exists


def test_recordmydesktop_installed(host):
    assert host.package('recordmydesktop').is_installed
